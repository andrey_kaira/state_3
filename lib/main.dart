import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'color.dart' as col;
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobx/mobx.dart';

void main() => runApp(Application());

final painter = Observable(0);

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark(),
      home: MyPage(),
    );
  }
}

class MyPage extends StatefulWidget {
  const MyPage({Key key}) : super(key: key);
  @override
  _MyPageState createState() => _MyPageState();
}

class _MyPageState extends State<MyPage> {


  @override
  Widget build(BuildContext context) {
    final _color = new Observable(col.Color());
    return Observer(builder: (ctx) {
      print(_color.value.selColor.value);
      return Scaffold(
        appBar: AppBar(
          title: Text('State Management Practice #2'),
        ),
        drawer: Drawer(
          child: Column(
            children: <Widget>[
              AppBar(
                automaticallyImplyLeading: false,
              ),
              InkWell(
                child: Container(
                  color: Colors.red,
                  height: 100.0,
                  margin: EdgeInsets.all(15.0),
                ),
                onTap: () => _color.value.changeColor(0),
              ),
              InkWell(
                child: Container(
                  color: Colors.yellow,
                  height: 100.0,
                  margin: EdgeInsets.all(15.0),
                ),
                onTap: () => _color.value.changeColor(1),
              ),
              InkWell(
                child: Container(
                  color: Colors.green,
                  height: 100.0,
                  margin: EdgeInsets.all(15.0),
                ),
                onTap: () => _color.value.changeColor(2),
              ),
            ],
          ),
        ),
        body: Container(
          color: _color.value.selColor.value,
        ),
      );
    });
  }
}
