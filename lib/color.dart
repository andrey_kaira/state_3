import 'package:mobx/mobx.dart';
import 'package:flutter/material.dart' as mat;

part 'color.g.dart';
class Color = _Color with _$Color;
abstract class _Color with Store {
  @observable
  Observable<mat.Color> selColor = Observable(mat.Colors.black);
  Color color;

  

  @action
  Color changeColor(int i) {
    switch (i) {
      case 0:
        selColor = Observable(mat.Colors.black);

        break;
      case 1:
        selColor = Observable(mat.Colors.red);
        break;
      case 2:
        selColor = Observable(mat.Colors.yellow);
        break;
      default:
        selColor = Observable(mat.Colors.green);
        break;
    }
    return color;
  }
}

